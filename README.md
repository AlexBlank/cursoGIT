# GITLAB NA PRÁTICA

## Autor: Alex Maass Blank

![](https://cdn-images-1.medium.com/max/1600/1*FtYgYG_G6rplUmF5fLzuXA.png)

### Tópicos abordados:

- Introdução sobre o GIT e GITLAB
- Baixando e instalando o GIT
- Criando conta no GITLAB
- Configurando usuário e e-mail do GITLAB no terminal
- Adicionando SSH KEY
- Criando novo projeto no GITLAB
- Clonando projeto do GITLAB
- Enviando projeto existente para o GITLAB
- Enviando novas atualizações do projeto para o GITLAB
- Sincronizando projeto remoto com local
- Criando readme.md
- Criando .gitignore
- A importância do comentário no commit
- A importância do git status
- A importância do git log
- "Desfazendo" os erros com git reset e git revert
- Trabalhando com Branch
- Versionamento com tags
- Clonando repositório público (clone e fork)
- Simplificando os comandos com “Alias”
- Apagando os arquivos e pastas (local e remotamente)
- Trabalho em equipe